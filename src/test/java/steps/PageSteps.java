package steps;

import com.codeborne.selenide.Selenide;
import io.cucumber.java.ru.И;

import static base.BasePage.initPage;

public class PageSteps {

    @И("^пользователь открывает стартовую страницу$")
    public void open() {
        System.setProperty("chromeoptions.args", "--remote-allow-origins=*");
        Selenide.open("https://youla.ru");
    }

    @И("^загружается страница \"(.+)\"$")
    public void loadPage(String pageName) {
        initPage(pageName);
    }

    @И("^ждем \"(.+)\" секунд$")
    public void userWait(long sec) throws InterruptedException {
        Thread.sleep(sec);
    }
}
