package base;

import meta.Page;
import pages.AutoPage;
import pages.MainPage;

public class AllPages {
    @Page("Главная")
    public MainPage mainPage = new MainPage();
    @Page("Легковые автомобили")
    public AutoPage autoPage = new AutoPage();

}
