package pages;

import base.BasePage;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import meta.PageElement;

import static com.codeborne.selenide.Selenide.$x;

//Страница Легковые автомобили
public class AutoPage extends BasePage {

    @PageElement("Легковые автомобили")
    public SelenideElement lightVehicles = $x("//span [text()='Легковые автомобили']");

    @PageElement("С пробегом")
    public SelenideElement withMileage = $x("//*[text()='С пробегом']");

    @PageElement("Марка")
    public SelenideElement carModel = $x("//* [text()='Марка']");

    @PageElement("Чекбокс Audi")
    public SelenideElement checkboxAudi = $x("//*[text()='Audi']/../div[1]");

}
