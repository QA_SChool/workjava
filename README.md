# Проект основан на Java

Чтобы запустить тест кейсы требуется поставить:

* File -> Projects Structure -> выбрать 11 Java

Перейти в папку и запустить:

* src-> test -> resources -> features -> TestAutoPage.


### Состав:

* src -> main -> java -> pages - в данной папке хранятся описанные страницы;
* src -> main -> java -> base -> класс AllPages - в данной папке хранится главный класс AllPages, котрый инициализирует все описанные страницы;
* src -> main -> java -> base -> BasePage - класс `BasePage` - описывает ключевой метод initPage();
* src -> main -> java -> base -> BaseWebElement - класс `BaseWebElement` - описывает ключевой метод getField();

* src-> test -> steps -> класс ElementSteps - описывает действия над элементами;
* src-> test -> steps -> класс Hooks - описывает предворительные действия перед тестом;
* src-> test -> steps -> класс PageSteps - описывает действия над страницей;

* src-> test -> resources -> features - в данной папке представлены наши тесты;
